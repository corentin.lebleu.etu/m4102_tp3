package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.dto.PizzaIngredientsCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaIngredientsDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest{
	
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
    private PizzaDao dao;
    private IngredientDao daoIng;
    
    @Override
    protected Application configure() {
    	BDDFactory.setJdbiForTests();
        return new ApiV1();
    }
	
    @Before
    public void setEnvUp() {
    	dao = BDDFactory.buildDao(PizzaDao.class);
        dao.createTableAndIngredientAssociation();
        daoIng = BDDFactory.buildDao(IngredientDao.class);
        daoIng.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
    	dao.dropTable();
    }
    
    @Test
    public void testGetEmptyList() {
    	dao.dropTable();
        // La méthode target() permet de préparer une requête sur une URI.
        // La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/pizzas").request().get();

        // On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        // On vérifie la valeur retournée (liste vide)
        // L'entité (readEntity() correspond au corps de la réponse HTTP.
        // La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
        // de la réponse lue quand on a un type paramétré (typiquement une liste).
        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
        });

        assertEquals(0, pizzas.size());
    }
    
    @Test
    public void testGetExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("PizzaTest");
		pizza.addIngredient(daoIng.findByName("mozzarella"));
		pizza.addIngredient(daoIng.findByName("tomate"));
		dao.insert(pizza);
		
		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();
			
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
			
		Pizza result = Pizza.fromDto(response.readEntity(PizzaIngredientsDto.class));
		assertEquals(pizza, result);
	}
    @Test
    public void testGetNotExistingIngredient() {
      Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    
    @Test
    public void testCreatePizza() {
        PizzaIngredientsCreateDto pizzaIngredientsCreateDto = new PizzaIngredientsCreateDto();
        pizzaIngredientsCreateDto.setName("Chorizo");
        pizzaIngredientsCreateDto.addIngredient(daoIng.findByName("mozzarella"));
        pizzaIngredientsCreateDto.addIngredient(daoIng.findByName("tomate"));
        pizzaIngredientsCreateDto.addIngredient(daoIng.findByName("lardons"));

        Response response = target("/pizzas").request().post(Entity.json(pizzaIngredientsCreateDto));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaIngredientsDto returnedEntity = response.readEntity(PizzaIngredientsDto.class);

        assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
        assertEquals(returnedEntity.getName(), pizzaIngredientsCreateDto.getName());
    }
    
    @Test
    public void testCreateSamePizza() {
        Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        pizza.addIngredient(daoIng.findByName("mozzarella"));
        pizza.addIngredient(daoIng.findByName("tomate"));
        pizza.addIngredient(daoIng.findByName("lardons"));
        dao.insert(pizza);

        PizzaIngredientsCreateDto pizzaIngredientsCreateDto = pizza.toIngredientCreateDto();
        Response response = target("/pizzas").request().post(Entity.json(pizzaIngredientsCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testCreatePizzaWithoutName() {
        PizzaIngredientsCreateDto pizzaIngredientsCreateDto = new PizzaIngredientsCreateDto();
        
        Response response = target("/pizzas").request().post(Entity.json(pizzaIngredientsCreateDto));
        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testDeleteExistingPizza() {
    	Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        pizza.addIngredient(daoIng.findByName("mozzarella"));
        pizza.addIngredient(daoIng.findByName("tomate"));
        pizza.addIngredient(daoIng.findByName("lardons"));
        dao.insert(pizza);

      Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

      Pizza result = dao.findByID(pizza.getId());
      assertEquals(result, null);
   }
    
    @Test
    public void testDeleteNotExistingPizza() {
      Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
    
    @Test
    public void testGetPizzaName() {
    	Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        pizza.addIngredient(daoIng.findByName("mozzarella"));
        pizza.addIngredient(daoIng.findByName("tomate"));
        pizza.addIngredient(daoIng.findByName("lardons"));
        dao.insert(pizza);

      Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

      assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

      assertEquals("Chorizo", response.readEntity(String.class));
   }
    
    @Test
    public void testGetNotExistingPizzaName() {
      Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

      assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
}

package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.dto.PizzaIngredientsCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaIngredientsDto;

public class Pizza {
	
	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> ingredients;
	
	public Pizza() {
		this.ingredients = new ArrayList<Ingredient>();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public void addIngredient(Ingredient ingredient) {
		this.ingredients.add(ingredient);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
    public String toString() {
        return "Pizza [id=" + id + ", name=" + name + "]";
    }
	
	public PizzaDto toDto() {
		PizzaDto dto = new PizzaDto();
		dto.setId(this.id);
		dto.setName(this.name);
		return dto;
	}
	public PizzaIngredientsCreateDto toIngredientCreateDto() {
		PizzaIngredientsCreateDto dto = new PizzaIngredientsCreateDto();
		dto.setName(this.name);
		for (Ingredient ing : this.ingredients) {
			dto.addIngredient(ing);
		}
		return dto;
	}
	
	public PizzaIngredientsDto toPizzaIngredientsDto() {
		PizzaIngredientsDto res = new PizzaIngredientsDto();
		res.setId(this.id);
		res.setName(this.name);
		for (Ingredient i : this.ingredients) {
			res.addIngredient(i);
		}
		return res;
	}
	
	public static Pizza fromDto(PizzaIngredientsDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());
		List<IngredientDto> ingredients = dto.getIngredients();
		for (IngredientDto i : ingredients) {
			pizza.addIngredient(i.toIngredient());
		}
		return pizza;
	}
	
	public static Pizza fromDto(PizzaIngredientsCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());
		List<IngredientDto> ingredients = dto.getIngredients();
		for (IngredientDto i : ingredients) {
			pizza.addIngredient(i.toIngredient());
		}
		return pizza;
	}
	
}

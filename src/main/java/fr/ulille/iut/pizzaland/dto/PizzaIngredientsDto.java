package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaIngredientsDto {
	
	private UUID id;
	private String name;
	private List<IngredientDto> ingredients;
	
	public PizzaIngredientsDto() {
		this.ingredients = new ArrayList<IngredientDto>();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<IngredientDto> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<IngredientDto> ingredients) {
		this.ingredients = ingredients;
	}
	
	public void addIngredient(Ingredient ingredient) {
		this.ingredients.add(Ingredient.toDto(ingredient));
	}
	
}

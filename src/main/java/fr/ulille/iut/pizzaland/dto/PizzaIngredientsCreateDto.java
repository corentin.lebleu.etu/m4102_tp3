package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaIngredientsCreateDto {
	
	private List<IngredientDto> ingredients;
	private String name;
	
	public PizzaIngredientsCreateDto() {
		this.ingredients = new ArrayList<IngredientDto>();
	}

	public List<IngredientDto> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<IngredientDto> ingredient) {
		this.ingredients = ingredient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addIngredient(Ingredient ingredient) {
		this.ingredients.add(Ingredient.toDto(ingredient));
	}
	
	
	
}

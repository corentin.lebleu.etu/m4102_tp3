package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class IngredientDto {

	private UUID id;
    private String name;

    public IngredientDto() {
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }
    
    public Ingredient toIngredient() {
    	Ingredient ingredient = new Ingredient();
    	ingredient.setName(this.name);
    	ingredient.setId(this.id);
    	return ingredient;
    }

}

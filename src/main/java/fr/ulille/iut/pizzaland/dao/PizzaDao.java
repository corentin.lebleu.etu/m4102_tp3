package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	 
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas_ingredients_assoc (id_pizza VARCHAR(128) NOT NULL, id_ingredient VARCHAR(128) NOT NULL, CONSTRAINT fk_pizza FOREIGN KEY (id_pizza) REFERENCES pizzas(id), CONSTRAINT fk_ingredient FOREIGN KEY (id_ingredient) REFERENCES ingredients(id),CONSTRAINT pk_assoc PRIMARY KEY (id_pizza, id_ingredient))")
	void createAssociationTable();

	@Transaction
	default void createTableAndIngredientAssociation() {
		createPizzaTable();
		createAssociationTable();
	}
	
	@SqlUpdate("DROP TABLE IF EXISTS pizzas_ingredients_assoc")
    void dropAssocTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS pizzas")
	void dropPizzaTable();
	
	@Transaction
	default void dropTable() {
		dropAssocTable();
		dropPizzaTable();
	}
	
	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();
	
	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByID(@Bind("id") UUID id);	
	
	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(@Bind("name") String name);	
	
	@SqlQuery("SELECT ing.id, ing.name FROM pizzas_ingredients_assoc AS assoc, ingredients AS ing WHERE ing.id = assoc.id_ingredient AND id_pizza = :id_pizza")
	@RegisterBeanMapper(Ingredient.class)
	List<Ingredient> getIngredientsOfPizzaById(@Bind("id_pizza") UUID id);
	
	@SqlUpdate("INSERT INTO pizzas VALUES (:id, :name)")
	void insertPizza(@BindBean Pizza pizza);
	
	@SqlUpdate("INSERT INTO pizzas_ingredients_assoc VALUES (:id_pizza, :id_ingredient)")
	void insertAssoc(@Bind("id_pizza") UUID id_pizza, @Bind("id_ingredient") UUID id_ingredient);
	
	@Transaction
	public default void insert(Pizza pizza) {
		insertPizza(pizza);
		UUID id_pizza = pizza.getId();
		for (Ingredient ing : pizza.getIngredients()) {
			insertAssoc(id_pizza, ing.getId());
		}
	}
	
	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	public void deletePizza(@Bind("id") UUID id);
	
	@SqlUpdate("DELETE FROM pizzas_ingredients_assoc WHERE id_pizza = :id")
	public void deleteAssoc(@Bind("id") UUID id);
	
	@Transaction
	public default void remove(UUID id) {
		deletePizza(id);
		deleteAssoc(id);
	}
	
}
	
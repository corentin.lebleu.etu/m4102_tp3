package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.dto.PizzaIngredientsCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaIngredientsDto;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces("application/json")
@Path("/pizzas")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());
	
	private PizzaDao pizzas;
	
	@Context
    public UriInfo uriInfo;

    public PizzaResource() {
        pizzas = BDDFactory.buildDao(PizzaDao.class);
        pizzas.createTableAndIngredientAssociation();
    }
	
    //Renvoie la liste des pizzas => nom + id
    @GET
    public List<PizzaDto> getAll() {
    	List<Pizza> liste = pizzas.getAll();
    	List<PizzaDto> res = new ArrayList<PizzaDto>();
    	for (Pizza p : liste) {
    		res.add(p.toDto());
    	}
    	return res;
    }
    
    //Renvoie une pizza => nom + id + ingredients
    @GET
    @Path("/{id}")
    @Produces({ "application/json", "application/xml" })
    public PizzaIngredientsDto getOnePizza(@PathParam("id") UUID id ) {
    	
    	//dao => getIngredientFromId(id_pizza) => renvoie List<Ingredient>
    	//Créer PizzaIngredientsDto et boucle pour ajouter chaque ingredient
    	
    	LOGGER.info("getOnePizza(" + id + ")");
    	
    	try {
    		List<Ingredient> ingredients = pizzas.getIngredientsOfPizzaById(id);
        	Pizza pizza = pizzas.findByID(id);
        	pizza.setIngredients(ingredients);
            return pizza.toPizzaIngredientsDto();
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }
    
    @POST
    public Response createPizza(PizzaIngredientsCreateDto pizzaIngredientCreateDto) {
    	if ((pizzaIngredientCreateDto.getIngredients() == null) || (pizzaIngredientCreateDto.getName()==null)) {
    		throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
    	}
    	
    	Pizza existing = pizzas.findByName(pizzaIngredientCreateDto.getName());
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }
        
        try {
            Pizza pizza = Pizza.fromDto(pizzaIngredientCreateDto);
            pizzas.insert(pizza);
            PizzaIngredientsDto pizzaIngredientDto = pizza.toPizzaIngredientsDto();

            URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();

            return Response.created(uri).entity(pizzaIngredientDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }
    
    @DELETE
    @Path("/{id}")
    public Response deletePizza(@PathParam("id") UUID id) {
    	if ( pizzas.findByID(id) == null ) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    	pizzas.remove(id);
    	return Response.status(Response.Status.ACCEPTED).build();
    }
    
    @GET
    @Path("/{id}/name")
    public String getPizzaName(@PathParam("id") UUID id) {
    	Pizza pizza = pizzas.findByID(id);
    	if (pizza == null) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
    	return pizza.getName();
    }
}

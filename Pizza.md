/pizzas					GET		<-application/json									Liste des pizzas (P3)
								<-application/xml			
								
/pizzas/{id}			GET		<-application/json									Pizza ou 404 (P4)
								<-application/xml		    

/pizzas/{id}/name		GET		<-text/plain										Nom de la pizza ou 404

/pizzas					POST	<-/->application/json					Pizza(P2) 	Nouvelle pizza (P4) / 406 si elle existe déjà
								->application/x-www-form-urlencoded

/pizzas/{id} 			DELETE


P2 => name, ingredients
P3 => id, name
P4 => id, name, ingredients
